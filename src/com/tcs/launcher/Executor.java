/**
 * 
 */
package com.tcs.launcher;

import java.io.IOException;
import java.sql.Connection;
import org.apache.log4j.Logger;
import com.tcs.util.BackupTablesUtility;
import com.tcs.util.ConnectionUtility;

/**
 * @author dev-dksc103198
 *
 */


public class Executor {

	final static Logger logger = Logger.getLogger(Executor.class);

	public static void main(String[] args) {
		
		logger.info("Data backing up started  ");
		taskExecutor();
		logger.info("Data backing up End ");
		
	}

	/**
	 * 
	 */
	public static void taskExecutor() {

		Connection con = ConnectionUtility.getConnection();
		try {
			BackupTablesUtility.getTables(con);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			Runtime.getRuntime().exec("cmd /c start c:\\mongo\\build.bat");
		} catch (IOException e) {
			logger.error("Error in executing batch command "+e.getMessage());
		}

	}

}
