package com.tcs.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;

public class BackupTablesUtility {

	final static Logger logger = Logger.getLogger(BackupTablesUtility.class);
	static Properties prop=ConnectionUtility.prop;

	static FileOutputStream fop = null;
	
	public static void getTables(Connection con) {

		logger.debug("Entry method getTables");
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(prop.getProperty("DB_DUMP_QUERY"));
			File batchFile=createBatFile();
			while (rs.next()) {
				write(con, rs.getString(1),batchFile);
				logger.debug("DB dump table >>" + rs.getString(1));
			}

			con.close();
		} catch (Exception e) {
			logger.fatal("Error in method getTables " + e.getMessage());

		}
		logger.debug("Exit method getTables");
	}

	public static void write(Connection con, String filename,File batchFile) {
		logger.debug("Entry method write");
		String csvFile = prop.getProperty("DB_DUMP_PATH") + filename + ".csv";
		logger.debug("CSV file url string " + csvFile);
		updateBatFile(filename,csvFile,batchFile);
		
		try {
			FileWriter writer = new FileWriter(csvFile);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from " + filename);
			Integer columncount = rs.getMetaData().getColumnCount();
			for (int i = 1; i <= columncount; i++) {
				writer.append(rs.getMetaData().getColumnName(i));
				if (i != columncount)
					writer.append(",");
			}

			writer.append("\n");
			while (rs.next()) {
				for (int i = 1; i <= columncount; i++) {
					writer.append(rs.getString(i));
					if (i != columncount)
						writer.append(",");

				}
				writer.append("\n");
			}
			writer.flush();
			writer.close();
			logger.debug("CSV generation for table " + filename + "is done in");
		} catch (Exception e) {
			logger.fatal("Exception in generating  " + filename + "is done in");
		}
		logger.debug("Exit method write");
	}

	public static void updateBatFile(String fileName, String csvFile,File file) {

	
		StringBuilder scriptUrl=new StringBuilder(100);
		scriptUrl.append(prop.getProperty("MONGO_IMPORT_SCRIPT_1")).
		append(prop.getProperty("MONGO_DB")).append(" -c ").append(fileName).append(" ").append(ConnectionUtility.prop.getProperty("MONGO_IMPORT_SCRIPT_2")).append(csvFile)
		.append(" --headerline").append("\n");
		try(FileOutputStream fop = new FileOutputStream(file,true);) {
			
			byte[] contentInBytes = scriptUrl.toString().getBytes();
			
			fop.write(contentInBytes);
			fop.flush();
			fop.close();
		} catch (IOException e) {
			logger.fatal("Exception in updating bat file >>"+scriptUrl);
		}
	}
	
	public static File createBatFile() {

		File file = new File("c:\\mongo\\build.bat");
		try(FileOutputStream fop = new FileOutputStream(file,true);) {
			file.createNewFile();
			fop.write(prop.getProperty("BATCH_DEFAULT_VALUES").getBytes());
			fop.write("\n".getBytes());
			fop.flush();
			fop.close();
		} catch (IOException e) {
			logger.fatal("Exception in creating bat file");
		}
		return file;
	}
	
	
	
}