/**
 * 
 */
package com.tcs.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * @author Ramesh
 *
 */

public class ConnectionUtility {

	static Properties prop = new Properties();

	static {
		try {
			InputStream input = new FileInputStream("c:\\mongo\\application.properties");
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	

	public static Connection getConnection() {
		Connection con = null;
		StringBuilder connectionUrl = new StringBuilder(100);
		connectionUrl.append(prop.getProperty("JDBC_URL_STRING")).append(prop.getProperty("HOST_NAME")).append("/")
				.append(prop.getProperty("SCHEMA_NAME"));
		try {
			Class.forName(prop.getProperty("DB_DRIVER"));
			con = DriverManager.getConnection(connectionUrl.toString(), prop.getProperty("DB_UNAME"),
					prop.getProperty("DB_PASS"));
		} catch (Exception e) {
			System.out.println(e);
		}
		return con;
	}
}
